#!/bin/bash
args=("$@")
function newPost {
	echo "Enter post title:"
	read postTitle
	echo "Enter post subtitle:"
	read postSub
	echo "Enter post cover picture:"
	read postCover
	#strip white spaces and single qouts 
	fileName="$(echo $postTitle | sed -e 's/[^a-zA-Z0-9]\+/-/g')"
	#add date format: Y-M-D
	fileName="$(date +'%Y-%m-%d-')${fileName}.markdown"
	fulldate="$(date +'%Y-%m-%d %H:%M:%S')"
	touch "_posts/${fileName}"
	#if no cover picture provided use default one
	if [ ! -f img/posts/${postCover} ]
		then
			postCover="default.png"
	fi
	#copy the post skeleton, net to fix this ugly indentation xD
	echo "---
layout:     post
title:      \"${postTitle}\"
subtitle:   \"${postSub}\"
date:       ${fulldate}
author:     \"Himmad zakaria\"
header-img: \"img/posts/${postCover}\"
---" > "_posts/${fileName}"
	#start the editor
	vim "_posts/${fileName}"
}

case $args in
	"new")  newPost;;
	"build") jekyll build ;;
	"serve") sudo jekyll server -H "0.0.0.0" -P 8080;;
esac

