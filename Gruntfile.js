module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            main: {
                src: 'js/clean-blog.js',
                dest: 'js/clean-blog.min.js'
            }
        },
        htmlmin: {                                     // Task
            dist: {                                      // Target
              options: {                                 // Target options
                removeComments: true,
                collapseWhitespace: true
              },
              files: [{                                  // Dictionary of files
                expand: true,
                cwd: '../dist',                             // Project root
                src: '**/*.html',                        // Source
                dest: '../dist'                            // Destination
              }]
            }
          },
        less: {
            expanded: {
                options: {
                    paths: ["css"]
                },
                files: {
                    "css/<%= pkg.name %>.css": "less/<%= pkg.name %>.less"
                }
            },
            minified: {
                options: {
                    paths: ["css"],
                    cleancss: true
                },
                files: {
                    "css/<%= pkg.name %>.min.css": "less/<%= pkg.name %>.less"
                }
            }
        },
        banner: 'rokin.github.io\n'+
        'autor: himmad zakaria\n'+
        'Built with:\n'+
        ' Jekyll\n'+
        ' Gruntjs\n'+
        ' Clean Blog v1.0.0 (http://startbootstrap.com)\n'+
        'Last build: <%= grunt.template.today %>',
        usebanner: {
            dist: {
                options: {
                    position: 'top',
                    banner: '<%= banner %>'
                },
                files: {
                    src: ['_site/human.txt']
                }
            }
        },
       // exec   ============================================================================================
        exec: {
            jekyllbuild: 'jekyll build'
       },       
       // concat ============================================================================================
        concat: {
            options: {
              separator: ';',
            },
            js: {
              src: ['js/jquery.min.js', 'js/bootstrap.min.js', 'js/clean-blog.min.js'],
              dest: 'js/script.min.js',
            },
            css: {
              src: ['css/bootstrap.min.css','css/clean-blog.min.css','css/syntax.css'],
              dest: 'css/styles.min.css',
            },
        },
        // copy configuration
        copy: {
          main: {
            files: [
              // makes all src relative to cwd
              {expand: true, cwd: '_site', src: ['**'], dest: '../dist/'},

            ]
          }
        },
        watch: {
            scripts: {
                files: ['js/<%= pkg.name %>.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                },
            },
            less: {
                files: ['less/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false,
                }
            },
        },
    });

    // Load the plugins.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-banner');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-exec');

    // Default task(s).
    grunt.registerTask('default', ['uglify', 'less', 'usebanner']);
    //grunt.registerTask('build',['copy:main','htmlmin:dist']);
    grunt.registerTask('build',['exec']);
    grunt.registerTask('minify:js',['uglify','concat:dist']);
    grunt.registerTask('minify:css',['concat:dist']);

};

